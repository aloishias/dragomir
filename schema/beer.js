const pgp = require("pg-promise")(/*options*/);
const db = pgp("postgres://username:password@host:port/database");
const Schema = pgp.Schema;

const BeerSchema = new pgp.Schema ({
    name: { type: String, unique: true, required: true },
    type: { type: String, unique: true, required: true },
    size: { type: String, required: true }
}, { collection: 'Biere' });

pgp.model('Biere', BeerSchema);
